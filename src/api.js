global.fetch = require("node-fetch");
const express = require("express");
const serverless = require("serverless-http");
const firebase = require("firebase");
const firebaseConfig = require("./config/config");

firebase.initializeApp(firebaseConfig);

const app = express();
const router = express.Router();


router.get("/", (req, res) => {
  res.json({
    hello: "hi!",
  });
});

app.use(`/.netlify/functions/api`, router);

module.exports = app;
module.exports.handler = serverless(app);
